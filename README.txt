DocumentAuthorChanger allows you to change the Author property of MS Word files in bulk. 
In order to run it, you'll need to have at least MS Word 2007 installed on your system.

Use of this application or its source code is at your own risk.

DocumentAuthorChanger is licensed under GPLv3.

(c) 2013 for DurfTeVragen: Joost Haverkort joost@joostit.com

https://www.facebook.com/groups/durftevragen/
